# ansible_role_docker_install
Role installs docker and docker-compose on Debian/Ubuntu hosts.

## Requirements

Tested on:
Debian 10, 11;
Ubuntu 20.04, 22.04

## Role Variables

### defaults/main.yml
```
docker_gpg_key_url: https://download.docker.com/linux/{{ ansible_lsb.id | lower }}/gpg
docker_gpg_key_path: /etc/apt/trusted.gpg.d/docker.asc
docker_repo_url: https://download.docker.com/linux/{{ ansible_lsb.id | lower }} {{ ansible_distribution_release }} stable
dpkg_architecture: amd64

# Choose whether to install requirements for docker_compose ansible module
docker_install_compose_module_req: true
```

## License

BSD-3-Clause
